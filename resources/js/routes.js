import Index from './components/Home.vue';
import Admin from './components/AdminIndex.vue';
import AllProduct from './components/AllProduct.vue';
import CreateProduct from './components/CreateProduct.vue';
import EditProduct from './components/EditProduct.vue';
 
const routes = [
    {
        name: 'index',
        path: '/',
        component: Index
    },
    {
        name: 'admin',
        path: '/admin',
        component: Admin
    },
    {
        name: 'home',
        path: '/admin/products',
        component: AllProduct
    },
    {
        name: 'create',
        path: '/admin/products/create',
        component: CreateProduct
    },
    {
        name: 'edit',
        path: '/admin/products/edit/:id',
        component: EditProduct
    }
];

export default routes;