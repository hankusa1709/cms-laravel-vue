import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    layout: 'home',
    title: ''
  },
  mutations: {
    SET_LAYOUT (state, payload) {
      state.layout = payload
    },
    SET_TITLE (state, payload) {
      state.title = payload
    }
  },
  getters: {
    layout (state) {
      return state.layout
    },
    title (state) {
      return state.title
    }
  },
  actions: {}
})

export default store;